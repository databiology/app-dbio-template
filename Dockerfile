#
#   CIAO Application Dockerfile
#

# dbio/base is based on Ubuntu LTS
# Please check for details: https://docs.databiology.net/tiki-index.php?page=Application%2BBase%2BImages
FROM dbio/base:5.0.1

## Contact details are not required as CIAO will project it:
# LABEL maintainer "FirstName LastName <name@example.com>"


 
## add required ubuntu packages, eg htop and curl:
# RUN    apt-get update -q=2 \
#    && apt-get upgrade -q=2 \
#    && apt-get install -yq=2 --no-install-recommends \
#        htop curl \
#    && apt-get clean \
#    && apt-get purge \
#    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


## entrypoint definition is not required as dbio/base defined it
## but copy main application script as /usr/local/bin/main.sh :
# COPY path/to/script     /usr/local/bin/main.sh
# RUN  chmod +x           /usr/local/bin/main.sh

