# CIAO Application Template

Fork this repository to start development of a new CIAO application.

* See the [CIAO documentation](https://docs.databiology.net/tiki-index.php?page=CIAO) for a description of the development process.

* See the [_cowsay_](https://bitbucket.org/databiology/app-dbio-cowsay) CIAO application example for reference.

